Problem Statements
-------------------

Problem 1: Write a program which accepts two inputs, representing two
playing cards out of a standard 52 card deck. Add these two cards
together to produce a blackjack score, and print the score on the screen
for the output.
 
Cards will be identified by the input, the first part representing the
face value from 2-10, plus A, K, Q, J. The second part represents the
suit S, C, D, H.
 
The blackjack score is the face value of the two cards added together,
with cards 2-10 being the numeric face value, and A is worth 11, and K,
Q, J are each worth 10.
 
Problem 2: Write a program that accepts a twitter username as input, and
returns the last 5 tweets, plus number of Tweets, Following, and
Followers for that twitter username.


Solution
--------
Problem 1: Blackjack.java has a main method which will call the performTask() method of the class 
for execution. 

Problem 2: TwitterSample.java has a main method to call performTask() method. 
 Dependency: 
 a. twitter4j library (included in the lib folder)
 b. Please enter your twitter API access codes in the CodeConstants class for the following constants:
    - ACCESS_TOKEN_SECRET 
	- ACCESS_TOKEN 
	- CONSUMER_SECRET 
	- CONSUMER_KEY 
  