package net.parmeet.homecode;

import net.parmeet.homecode.common.CodeConstants;
import net.parmeet.homecode.common.ConsoleUtil;

/**
 * Class to
 * <ul>
 * <li> Accept two cards from standard 52 card pack</li>
 * <li> Print the Blackjack score from drawn cards</li>
 * </ul>
 * @author Parmeet
 *
 */
public class Blackjack {
	
	private String card1=null;
	private String card2=null; 
	
	/**
	 * Get the numeric face value of a card  
	 * @param card 
	 * @return integer value from 2-11
	 */
	public int getCardValue(String card){
		int value=0;
		String faceValue= null;
		//Discard suit Character to process face value
		faceValue = card.replaceAll("[S|C|D|H]", CodeConstants.BLANK);
		
		//Change photo cards to number value
		faceValue = faceValue.replaceAll("[J|Q|K]", "10");
		faceValue = faceValue.replaceAll("A", "11");
		
		//Get the integer value of card
		try{
			value = Integer.parseInt(faceValue);
		}catch(NumberFormatException e){
			//Consume exception
		}
		
		if(value>11){
			value =0;
		}
		
		return value;
	}

	/**
	 * Get a valid card input from console
	 * Also ensures that duplicate cards cannot be drawn
	 * @return
	 */
	protected String getValidCard(){
		String card=null;
		do{
			card = ConsoleUtil.consoleInput("Enter "+ ((card1!=null)?"second":"first") +" card <facevalue 2-10/J/Q/K/A><Suit S/C/D/H> : ");
			if(card!=null){
				//Remove spaces and convert to upper case for ease of processing
				card = card.replaceAll(CodeConstants.SPACE, CodeConstants.BLANK).toUpperCase();
				if(CodeConstants.BLANK.equals(card)){
					card=null;
				}else {
					if(!(card.endsWith(CodeConstants.SPADE) || card.endsWith(CodeConstants.CLUB) || card.endsWith(CodeConstants.DIAMOND) || card.endsWith(CodeConstants.HEARTS))){
						card = null;
						ConsoleUtil.consoleWrite("Invalid Suit, must end with S/C/D/H retry. \n");
					}else{
						if(getCardValue(card)<1){
							card=null;
							ConsoleUtil.consoleWrite("Invalid card, retry. \n");
						}

						//If card1 is already drawn new card cannot be same as first card
						if(card1!=null && card1.equalsIgnoreCase(card)){
							ConsoleUtil.consoleWrite("Card " + card1 + " is already drawn, please enter another card.");
							card = null;
						}
					}
				}
			}
		}while(card==null);
		
		return card;
	}

	public void performTask(){
		card1 = getValidCard();
		card2 = getValidCard();
				
		ConsoleUtil.consoleWrite("Blackjack score for " + card1 + " & " + card2+ " is:" + (getCardValue(card1)+getCardValue(card2)));
	}
	
	public static void main(String[] args) {
		(new Blackjack()).performTask();
	}
	
}
