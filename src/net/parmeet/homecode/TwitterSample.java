package net.parmeet.homecode;

import net.parmeet.homecode.common.CodeConstants;
import net.parmeet.homecode.common.ConsoleUtil;
import twitter4j.IDs;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Class that 
 * <ol>
 * <li> Accepts a twitter username as input, and </li>
 * <li> Returns the last 5 tweets,</li>
 * <li> Number of Tweets, </li>
 * <li> Following, and</li>
 * <li> Followers for that twitter username.</li>
 * </ol> 
 * @author Parmeet
 * @see ConsoleUtil
 */
public class TwitterSample {
	private Twitter twitter=null;

	/**
	 * Default constructor initializes Twitter object and does Auth
	 */
	public TwitterSample(){
		//Initialize Twitter Object when instance is created
		initTwitter4j();
	}

	/**
	 * Initialize Twitter Object using credentials
	 */
	public void initTwitter4j(){
		ConfigurationBuilder cb;
		cb = new ConfigurationBuilder();
		cb.setOAuthConsumerKey(CodeConstants.CONSUMER_KEY)
		.setOAuthConsumerSecret(CodeConstants.CONSUMER_SECRET)
		.setOAuthAccessToken(CodeConstants.ACCESS_TOKEN)
		.setOAuthAccessTokenSecret(CodeConstants.ACCESS_TOKEN_SECRET);

		// gets Twitter instance with default credentials
		twitter = new TwitterFactory(cb.build()).getInstance();

	}

	/**
	 * Default to 5 tweets page size
	 * @param userName
	 * @throws TwitterException
	 */
	public ResponseList<Status> getUserTweets(String userName) throws TwitterException{
		return getUserTweets(userName, 5);
	}

	/**
	 * Fetch recent tweets for a given handle
	 * @param userName
	 * @param pageSize
	 * @return
	 * @throws TwitterException
	 */
	public ResponseList<Status> getUserTweets(String userName, int pageSize) throws TwitterException{
		ResponseList<Status> timeLine = null;
		Paging page = new Paging(1, pageSize);
		if(twitter!=null){
			timeLine = twitter.getUserTimeline(userName, page);
		}
		return timeLine;
	}

	/**
	 * Find Username of Twitter
	 * @param userName
	 * @return
	 * @throws TwitterException
	 */
	public User getUser(String userName) throws TwitterException{
		User user=null;
		if(twitter!=null){
			user = twitter.showUser(userName);
		}
		return user;
	}
	
	/**
	 * Show user's followers and ask for confirmation if follower count is over given threshold
	 * @param user
	 * @param threshold to allow restrict multiple calls to Twitter
	 * @throws TwitterException
	 */
	public void showFollowersForUser(User user, int threshold) throws TwitterException{
		IDs followerIds=null;
		boolean showAllFollowers = true;
		User follower=null;
		int shownUsers=0;
		
		followerIds = twitter.getFollowersIDs(user.getScreenName(), -1);
		if(user.getFollowersCount()>threshold){
			showAllFollowers = ConsoleUtil.getUserConfirmation("@" + user.getScreenName() + " has more than " + threshold + " followers, show all of them?" );
		}
		
		ConsoleUtil.consoleWrite("Followers: ");
		for(long id: followerIds.getIDs()){
			if(!showAllFollowers && shownUsers>=threshold){
				break;
			}
			follower = twitter.showUser(id);
			ConsoleUtil.consoleWrite(++shownUsers + ". [@" + follower.getScreenName() + "] " + follower.getName());
		}
	}
	
	/**
	 * Perform sample tasks
	 */
	public void performTask(){
		String userName = null;
		ResponseList<Status> tweetsForUser=null;
		User user = null;

		userName = ConsoleUtil.consoleInput("Please enter twitter name (do not enter @) : ");
		if(userName==null || CodeConstants.BLANK.equals(userName.trim())){
			ConsoleUtil.consoleWrite("No input ... exiting");
		}
		
		ConsoleUtil.consoleWrite("Finding @" + userName + " on Twitter ...");
		try {
			user = this.getUser(userName);
			ConsoleUtil.consoleWrite("\n\nTweets so far: " + user.getStatusesCount()+"\n\n");
			
			if(user.getStatusesCount()>0){
				tweetsForUser = this.getUserTweets(user.getScreenName());
				
				if(tweetsForUser!=null && tweetsForUser.size()>0){
					ConsoleUtil.consoleWrite("Last " + tweetsForUser.size() + " tweets: ");
				}
				
				for (Status status : tweetsForUser) {
					ConsoleUtil.consoleWrite("@" + status.getUser().getScreenName() + ":" + status.getText());
				}
			}
			
			ConsoleUtil.consoleWrite("\n\n@" + user.getScreenName() + " has " + user.getFollowersCount() + " followers.");
			
			if(user.getFollowersCount()>0){
				showFollowersForUser(user, 10);
			}
			
		} catch (TwitterException e) {
			if(e.getStatusCode()==404){
				ConsoleUtil.writeConsoleError("Invalid twitter handle: @" + userName);
			}else if(e.getErrorCode()==88){
				ConsoleUtil.writeConsoleError("Too Many twitter requests, please try again later !!");
			}
			else{
				ConsoleUtil.writeConsoleError("Something went wrong: " + e.getMessage());
			}
		}
		
	}

	public static void main(String[] args) {
		(new TwitterSample()).performTask();
	}
}
