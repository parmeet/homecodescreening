package net.parmeet.homecode.common;

/**
 * Constants Class
 * @author Parmeet
 */
public class CodeConstants {
	public static final String BLANK = "";
	public static final String SPACE = " ";
	public static final String YES   = "y";
	public static final String NO    = "n";
	
	//Blackjack constants
	public static final String HEARTS = "H";
	public static final String DIAMOND = "D";
	public static final String CLUB  = "C";
	public static final String SPADE = "S";
	
	//Twitter API Access codes
	public static final String ACCESS_TOKEN_SECRET = "<your_access_token_secret>";
	public static final String ACCESS_TOKEN = "<your_access_token>";
	public static final String CONSUMER_SECRET = "<your_consumer_secret>";
	public static final String CONSUMER_KEY = "<your_consumer_key>";
}
