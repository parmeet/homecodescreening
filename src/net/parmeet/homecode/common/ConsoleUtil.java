package net.parmeet.homecode.common;

import java.util.Scanner;

/**
 * Util class for console operations		
 * @author Parmeet
 */
public class ConsoleUtil {
	
	/**
	 * Util method to write a string to console
	 * @param message
	 */
	public static void consoleWrite(String message){
		System.out.println(message);
	}
	
	/**
	 * Write to consle error
	 * @param message
	 */
	public static void writeConsoleError(String message){
		System.err.println(message);
	}
	
	/**
	 * Get user input from console
	 * @param message
	 * @return userInput
	 */
	@SuppressWarnings("resource")
	public static String consoleInput(String message){
		String input=null;
		Scanner scanner=null;
		consoleWrite(message);
		try{
			scanner = new Scanner(System.in); 
			input = scanner.nextLine();  
		}finally{
			//set scanner to null, standard input shouldn't be closed
			scanner=null;
		}
		return input;
	}
	
	/**
	 * Get user confirmation from console
	 * @param message
	 * @return boolean
	 */
	public static boolean getUserConfirmation(String message){
		String answer = null;
		boolean confirmation = false;
		//Ask user to enter Y or N
		do{
			answer = consoleInput(message + " [y/n]");
		}while(!CodeConstants.YES.equalsIgnoreCase(answer) && !CodeConstants.NO.equalsIgnoreCase(answer));
		if(CodeConstants.YES.equalsIgnoreCase(answer)){
			confirmation = true;
		}
		
		return confirmation;
	}
}
